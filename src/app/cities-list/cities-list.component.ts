import { Component, OnInit } from '@angular/core';
import { WeatherApiService } from '../services/weather-api.service';

@Component({
  selector: 'app-cities-list',
  templateUrl: './cities-list.component.html',
  styleUrls: ['./cities-list.component.css']
})
export class CitiesListComponent implements OnInit {

  storageArray:any[] = [];
  editeMode:boolean;
  weatherItems:any[] = [];

  constructor(private api:WeatherApiService) { }

  ngOnInit() {

    this.storageArray = this.api.getItemsFromStorage();

    for( let i = 0; i< this.storageArray.length; i++ ) {

      this.api.getWeather(this.storageArray[i].name)
        .subscribe(data => this.weatherItems.push(data))

    }

    this.api.storeChanged.subscribe(

        (store) => {

            this.weatherItems = store;

            if(this.storageArray.length != 0) {

                this.editeMode = true;
               

            }

            else {

              this.editeMode = false;
              

            }

        }
    );

  	if(this.storageArray.length != 0) {

  		  this.editeMode = true;

  	}

    else {

      this.editeMode = false;
 

    }

  }

}
