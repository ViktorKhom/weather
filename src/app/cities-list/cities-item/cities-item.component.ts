import { Component, OnInit, Input } from '@angular/core';
import { WeatherApiService } from '../../services/weather-api.service';

@Component({
  selector: 'app-cities-item',
  templateUrl: './cities-item.component.html',
  styleUrls: ['./cities-item.component.css']
})
export class CitiesItemComponent implements OnInit {

  showSpinner: boolean = true;
  @Input() itemData;
  @Input() itemIndex;

  constructor( private api:WeatherApiService ) { }

  ngOnInit() {

  	setTimeout(() => {  

      this.showSpinner = false;

    }, 1500)
  	
  }

  deleteCityFromStorage() {

  	this.api.deleteItemFromStorage(this.itemIndex);

  }

}
