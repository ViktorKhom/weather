import { Routes } from '@angular/router';
import { WeatherComponent } from './weather/weather.component';
import { CitiesListComponent } from './cities-list/cities-list.component';

export const appRoutes: Routes = [
    { path: 'weather', component: WeatherComponent },
    { path: 'my-cities', component: CitiesListComponent },
    { path: '', redirectTo: '/weather', pathMatch: 'full'}
];
