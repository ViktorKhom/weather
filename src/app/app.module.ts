import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { WeatherApiService } from './services/weather-api.service';
import { LocalStorageModule } from 'angular-2-local-storage';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { WeatherListComponent } from './weather/weather-list/weather-list.component';
import { WeatherItemComponent } from './weather/weather-list/weather-item/weather-item.component';
import { WeatherSearchComponent } from './weather/weather-search/weather-search.component';
import { CitiesListComponent } from './cities-list/cities-list.component';
import { WeatherComponent } from './weather/weather.component';
import { appRoutes } from './routes';
import { CitiesItemComponent } from './cities-list/cities-item/cities-item.component';
import { SpinnerComponent } from './spinner/spinner.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    WeatherListComponent,
    WeatherItemComponent,
    WeatherSearchComponent,
    CitiesListComponent,
    WeatherComponent,
    CitiesItemComponent,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    LocalStorageModule.withConfig({
          prefix: 'my-app',
          storageType: 'localStorage'
    })
  ],
  providers: [WeatherApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
