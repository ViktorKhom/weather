import { Injectable, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs';


@Injectable()
export class WeatherApiService {

	localStorageArray:any[] = [];

	weatherItems:any[] = [];

	api:string = '&units=metric&appid=ca633d14fb0de83403f0a6083e373f24';

	url:string = 'http://api.openweathermap.org/data/2.5/weather?q=';

	storeChanged = new Subject<any>();

	constructor( private http:Http ) {}

	// methods for array in localStorage

	getItemsFromStorage() {

		let dataFromStorage = localStorage.getItem('cities');

	  	let array = JSON.parse(dataFromStorage);

	  	this.localStorageArray = array;

	  	if( this.localStorageArray == undefined ) {

	  		this.localStorageArray = [];

	  	}
 
		return this.localStorageArray;

	}

	addItemToStorage(item) {

		this.localStorageArray.push(item);

		let string = JSON.stringify(this.localStorageArray);

		this.storeChanged.next(this.localStorageArray.slice());

		localStorage.setItem('cities', string);

	}

	deleteItemFromStorage(index) {

		this.localStorageArray.splice(index,1);

		let string = JSON.stringify(this.localStorageArray);

		this.storeChanged.next(this.localStorageArray.slice());

		localStorage.setItem('cities', string);

	}
	
	// methods for array in current momment

	getWeatherItems() {

		return this.weatherItems;

	}

	addWeatherItem(item) {
		
		this.weatherItems.push(item);

	}

	deleteWeatherItem(index) {

		this.weatherItems.splice(index,1);

	}

	getWeather(city) {

		return this.http.get(this.url+city+this.api)
				.map( 

					(res)=> {

						return res.json()
						
					}
				)
				.catch(error => {

					return Observable.throw(error.json());

				});
	}

}
