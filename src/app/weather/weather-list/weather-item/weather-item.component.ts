import { Component, OnInit, Input } from '@angular/core';
import { WeatherApiService } from '../../../services/weather-api.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-weather-item',
  templateUrl: './weather-item.component.html',
  styleUrls: ['./weather-item.component.css']
})
export class WeatherItemComponent implements OnInit {

  constructor(private api:WeatherApiService) {

  }

  firstItem:boolean = false;
  showSpinner: boolean = true;
  @Input() itemData;
  @Input() itemIndex;

  ngOnInit() {

    setTimeout(() => {  

      this.showSpinner = false;

    }, 1500)

    if(this.itemIndex == 0) {

      this.firstItem = true;

    }

    else {

      this.firstItem = false;

    }

  }

  saveCity() {

  	let items = this.api.getWeatherItems();

    this.api.addItemToStorage(items[this.itemIndex]);

    alert('City saved !')

  }

  deleteCity() {

  	this.api.deleteWeatherItem(this.itemIndex);

  }

}
