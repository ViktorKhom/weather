import { Component, OnInit } from '@angular/core';
import { WeatherApiService } from '../../services/weather-api.service';


@Component({
  selector: 'app-weather-list',
  templateUrl: './weather-list.component.html',
  styleUrls: ['./weather-list.component.css']
})
export class WeatherListComponent implements OnInit {

  weatherItems:any[] = [];
  
  constructor(private api:WeatherApiService) { }

  ngOnInit() {

  	this.weatherItems = this.api.getWeatherItems();

  	if(this.weatherItems.length < 1) {
  		
		this.api.getWeather('London').subscribe(

	        city => {

	           this.api.addWeatherItem(city);
	           this.weatherItems = this.api.getWeatherItems();

	        },

	        error =>  {

	            console.log(error);

	        }
	    );

  	}
  	
  }
}
