import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WeatherApiService } from './../../services/weather-api.service';
import { Subject } from 'rxjs/Subject';
import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'app-weather-search',
  templateUrl: './weather-search.component.html',
  styleUrls: ['./weather-search.component.css']
})
export class WeatherSearchComponent implements OnInit {

  constructor(private api:WeatherApiService, 
              private router: Router, 
              private route: ActivatedRoute,
              private storage: LocalStorageService) { }

  onSubmit(city) {
  	  
      this.api.getWeather(city.value).subscribe((item) => {

        const weatherItem = item;
        this.api.addWeatherItem(weatherItem);

    });

  }


  ngOnInit() {

  }

}
